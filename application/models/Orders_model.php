<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_model extends CI_Model {

  public function __construct() {
    // Call the CI_Model constructor
    parent::__construct();
  }

  /**
 	 * Defining methods for orders models
 	 */

  /**
 	 * Create an order into the database
 	 */
  public function create_order($user_id = '', $name = '', $price = '', $quantity = '') {
    $data = array(
      'user_id' => $user_id,
      'status' => 'created'
    );
    $this->db->insert('orders',$data);

    $data = array(
      'order_id' => $this->db->insert_id(),
      'name' => $name,
      'price' => $price,
      'quantity' => $quantity
    );
    $this->db->insert('order_items',$data);
  }

  /**
 	 * Get an order by it's id from database
 	 */
  public function get_order_by_id($id = '') {
    $this->db->select('*');
    $this->db->from('orders o');
    $this->db->join('users u', 'u.id = o.user_id', 'INNER');
    // $this->db->join('Soundtrack c', 'c.album_id=a.album_id', 'left');
    $this->db->where('o.id', $id);
    $this->db->order_by('o.created_at','DESC');
    $query = $this->db->get();
    return $query->result();
  }

  /**
 	 * Get all orders of today from database
 	 */
  public function get_today_orders() {
    $query = $this->db->query("SELECT *
      FROM order_items
      INNER JOIN orders ON order_items.order_id = orders.id
      WHERE Date(order_items.created_at) = CURDATE() OR Date(orders.created_at) = CURDATE()");
    return $query->result();
  }

  /**
 	 * Get all orders of an user
   * from database order by created date desc
 	 */
  public function get_orders_by_user_id($user_id) {
    $this->db->select('*');
    $this->db->from('orders o');
    $this->db->join('users u', 'u.id = o.user_id', 'INNER');
    $this->db->where('o.user_id', $user_id);
    $this->db->order_by('o.created_at','DESC');
    $query = $this->db->get();
    return $query->result();
  }

  /**
 	 * Update order status after successfull payment
   * into orders table in the database
 	 */
  public function update_order_payment($id) {
    $this->status = 'processed';
    $this->db->update('orders', $this, array('id' => $id));
    return $this->db->affected_rows();
  }

  /**
 	 * Update order status to cancelled
   * into orders table in the database
   * for cancelling the order
 	 */
  public function cancel_order($id) {
    $this->status = 'cancelled';
    $this->db->update('orders', $this, array('id' => $id));
    return $this->db->affected_rows();
  }
}
