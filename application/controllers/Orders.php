<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

	/**
	 * Constom constructor function
	 * to load our orders_model for every route
	 */
	public function __construct() {
		// Call the CI_Model constructor
		parent::__construct();
		// Load Orders Model
		$this->load->model('orders_model');
	}

	/**
	 * All function and urls releted to Orders
	 */

	/**
	 * Defining Index route for orders
	 */
	public function index($id = '', $route = '') {
		// Getting the type of request GET, POST, PUT in lower case
		$request_type = $this->input->method();
		switch ($request_type) {
			// Processing POST request
			case 'post':
				// Fetching data from input
				$user_id = $this->input->post('user_id');
				$name = $this->input->post('name');
				$price = $this->input->post('price');
				$quantity = $this->input->post('quantity');
				// Calling methods from order_model
				$this->orders_model->create_order($user_id, $name, $price, $quantity);
				break;

			// Processing DELETE, PUT request
			case 'delete':
			case 'put':
				if ($id) {
					// Calling helper methods of Orders controller
					$order = $this->update_order($id, $route);
					// Calling helper methods of Orders controller to send JSON response
					$this->send_json(200, "Order is updated", $order);
				} else {
					// Calling helper methods of Orders controller to send JSON response
					$this->send_json(401, "Something is missing");
				}
				break;

			// Processing GET request
			default:
				if ($id) {
					// Calling methods from order_model
					$order = $this->orders_model->get_order_by_id($id);
					// Calling helper methods of Orders controller to send JSON response
					$this->send_json(200, "Data is found", $order);
				} else {
					// Calling helper methods of Orders controller to send JSON response
					$this->send_json(403, "Get Request Not Allowed.");
				}
		}
	}

	/**
	 * Defining Search route for orders
	 */
	public function search() {
		$user_id = $this->input->get('user_id');
		if ( ! $user_id ) {
			// Calling helper methods of Orders controller to send JSON response
			return $this->send_json(401, "Something is missing");
		}
		// Calling methods of orders_model
		$orders = $this->orders_model->get_orders_by_user_id($user_id);
		// Calling helper methods of Orders controller to send JSON response
		$this->send_json(200, "Data is found", $orders);
	}

	/**
	 * Defining today route for orders
	 * to get all orders created today
	 */
	public function today() {
		// Calling methods from order_model
		$orders = $this->orders_model->get_today_orders();
		// Calling helper methods of Orders controller to send JSON response
		$this->send_json(200, "Data is found", $orders);
	}

	/**
	 * Private methods declaration for orders
	 * these methods can not be accessed from
	 * public requests
	 *
	 * These are defined as helper methods
	 * to serve public requests
	 */

	/**
 	 * Helper method for updating orders
 	 */
	private function update_order($id, $route) {
		switch ($route) {
			// To cancel an order
			case 'cancel':
				// Calling methods from order_model
				$status = $this->orders_model->cancel_order($id);
				break;
			// Processing payment request
			case 'payment':
				// Calling methods from order_model
				/**
				 * I don't found any payment option in the given attachemnt
				 * so I used order status to be processed on this opetion.
				 * I hope that is what you want for this option.
				 */
				$status = $this->orders_model->update_order_payment($id);
				break;
			// To update order and order attribute
			default:
				// Calling methods from order_model
				/**
				 * I don't understand this option
				 * So I left it un implemented
				 */
				$status = $this->orders_model->update_order_and_order_attribute($id);
		}
		// returning status of the update of order
		return $status;
	}

	/**
 	 * Helper method to send JSON output
	 * to the request for all orders route
 	 */
	private function send_json($status = 401, $message = 'No Data Found', $data = '') {
		// Setting response status code
		$this->output->set_status_header($status, $message);
		if ($data) {
			$res['data'] = $data;
			$output_str = json_encode($res);
			// Setting data to the response
			$this->output->set_output($output_str);
		}
		return;
	}
}
